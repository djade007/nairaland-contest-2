<?php

class TaxController extends BaseController{

    public function postStep1() {
        $validator = Validator::make(Input::all(), Tax::$step1_rules);

        if($validator->passes()) {

            $data = array(
                'form_no' => Input::get('no'),
                'step' => 2
            );
            Return Redirect::to('/')->with($data);
        } else {
            Return Redirect::to('/')->withErrors($validator);
        }
    }

    public function postStep2() {
        $validator = Validator::make(Input::all(), Tax::$step2_rules);
        $count = count(Input::all());
        if($validator->passes()) {

            // define required variable
            $val = '';
            $no_of_appliances = '';
            $total_watt = '';
            for($i=0; $i<$count; $i++) {
                if (Input::has('appliance_'.$i)) {
                    $val += 0.1 * Input::get('appliance_'.$i);

                    // additional info
                    $no_of_appliances += 1;
                    $total_watt += Input::get('appliance_'.$i);
                }
            }

            $salary = Auth::user()->salary;
            // tax is 5% of salary + $val
            $total = 0.05 * $salary + $val;

            // save tax info to database
            // get the user id
            $user_id = Auth::id();

            // update or create a new tax if no previous tax is found
            $tax = Tax::firstOrNew(array('user_id' => $user_id));
            $tax->user_id = $user_id;
            $tax->no_of_appliances = $no_of_appliances;
            $tax->total_watt = $total_watt;
            $tax->tax = $total;
            $tax->save();


            $data = array(
                'tax' => $total,
                'step' => 3
            );
            Return Redirect::to('/')->with($data);
        } else {
            Input::flash();
            $data = array(
                'form_no' => $count -1,
                'step' => 2
            );
            Return Redirect::to('/')->with($data)->withErrors($validator);
        }
    }
} 