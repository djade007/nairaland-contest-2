<?php


class UserController extends BaseController{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct() {

        /**       Filters        **/
        // make sure guest does not have access to account index and logout
        $this->beforeFilter('auth', array('only' => array('getIndex', 'getLogout')));

        // make sure logged user cant access login and create account pages
        $this->beforeFilter('user', array('only' => array('getLogin', 'getRegistration', 'getRemind', 'postRemind', 'getReset', 'postReset')));

        // laravel filter to secure forms
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function getIndex() {
        //data to be passed to view
        $data = array(
            'title' => 'Edit Account',
            'active' => 'account',
        );
        return View::make('home.account', $data);
    }

    public function getLogin() {
        //data to be passed to view
        $data = array(
            'title' => 'Login',
            'active' => 'account',
        );
        return View::make('forms.login', $data);
    }

    public function getRegistration() {
        //data to be passed to view
        $data = array(
            'title' => 'Create an Account',
            'active' => 'account',
        );
        return View::make('forms.registration', $data);
    }

    public function getLogout() {
        // Logout user
        Auth::logout();
        return Redirect::to('/');
    }

    // Handle the forms
    public function postLogin() {

        $email = Input::get('email');
        $password = Input::get('password');
        $remember = Input::has('remember');

        // attempt to login user with above detail
        if (Auth::attempt(array('email' => $email, 'password' => $password), $remember)){
            return Redirect::to('/');
        }else {
            // login failed
            // return only last entered email
            Input::flashOnly('email');
            return Redirect::action('UserController@getLogin')->withErrors('failed');
        }

    }

    public function postRegistration() {
        $validator = Validator::make(Input::all(), User::$rules, User::$messages);

        if ($validator->fails()) {
            Input::flash();
            return Redirect::action('UserController@getRegistration')->withErrors($validator);
        } else {
            //Save user to database
            $user = new User;
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->sex = Input::get('sex');
            $user->salary = Input::get('salary');
            $user->occupation = Input::get('occupation');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password1'));
            $user->save();

            // Pass $message as success
            return Redirect::to('/')->with('message','success');
        }
    }

    public function postIndex() {
        $validator = Validator::make(Input::all(), User::$rules1, User::$messages);

        if ($validator->fails()) {
            return Redirect::action('UserController@getIndex')->withErrors($validator);
        } else {
            // edit user and save to database
            $user = User::find(Auth::id());
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->sex = Input::get('sex');
            $user->salary = Input::get('salary');
            $user->occupation = Input::get('occupation');
            // check if user wants to change password before saving the new one
            if(Input::has('password1')) {
                $user->password = Hash::make(Input::get('password1'));
            }

            $user->save();

            // Pass $message as success
            return Redirect::action('UserController@getIndex')->with('message','success');
        }
    }


    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind()
    {
        //data to be passed to view
        $data = array(
            'title' => 'Forgot password',
            'active' => 'account',
        );
        return View::make('password.remind', $data);
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind()
    {
        switch ($response = Password::remind(Input::only('email')))
        {
            case Password::INVALID_USER:
                return Redirect::back()->with('errors', Lang::get($response));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('status', Lang::get($response));
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) App::abort(404);

        //data to be passed to view
        $data = array(
            'title' => 'Edit Account',
            'active' => 'account',
        );
        return View::make('password.reset', $data)->with('token', $token);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset()
    {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);

            $user->save();
        });

        switch ($response)
        {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()->with('errors', Lang::get($response));

            case Password::PASSWORD_RESET:
                return Redirect::to('/')->with('status', 'Your password has been changed successfully');
        }
    }
} 