{{-- show user detail and logout link --}}
<div class="panel panel-success pull-right">
    <div class="panel-heading">
        <h3 class="panel-title">User</h3>
    </div>
    <div class="panel-body">
        <h5>You are logged in as <a href="{{ action('UserController@getIndex') }}">{{ Auth::user()->first_name .' '. Auth::user()->last_name}}</a></h5>
        <h5>Your current salary is ₦{{ Auth::user()->salary}}</h5>
        {{-- get tax details--}}
        <?php $tax = Tax::find(Auth::id());?>
        @if($tax)
        <h5>Your current tax is ₦{{ $tax->tax }} on {{ $tax->no_of_appliances }} appliance(s),<br>Total of {{ $tax->total_watt }}watt</h5>
        @else
        <h4>You are yet to calculate your first tax</h4>
        @endif
        <h5><a href="{{ action('UserController@getLogout') }}">Logout</a> </h5>
    </div>
</div>