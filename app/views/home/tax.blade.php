
@unless(Session::has('step'))
    <h2><small>Let's start.........</small></h2>
    <h2 class="text-success">Step 1:</h2>
    <h3>How many electronic appliances do you have? <small>maximum of 15</small></h3>

    @if(Session::has('errors'))
        <div class="alert alert-warning" role="alert">The following errors occurred</div>
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif

    {{ Form::open(array('action' => 'TaxController@postStep1', 'role' => 'form', 'class' => 'form-inline')) }}
        <div class="form-group">
            <label for="no">Number Of Electronic Appliance(s)</label>
            <input type="number" class="form-control" name="no" id="no">
        </div>
        <button type="submit" class="btn btn-default">Next Step</button>
    {{ Form::close() }}

@else

<?php $step = Session::get('step'); ?>


{{-- handle step 2 --}}
@if($step == 2)
    <h2 class="text-success">Step 2:</h2>

    @if(Session::has('errors'))
    <div class="alert alert-warning" role="alert">The following errors occurred</div>
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </div>
    @endif

    <?php $form_no = Session::get('form_no') +1; ?>
    <h3>Enter the watt of your appliances</h3>

    {{ Form::open(array('action' => 'TaxController@postStep2', 'role' => 'form')) }}
        @for ($no = 1; $no < $form_no ; $no++)
            <div class="row">

                <div class="form-group">
                    {{ Form::label('appliance_'.$no, 'Appliance '.$no) }}
                    <div class="input-group">
                        <input type="text" value="{{ Input::old('appliance_'.$no) }}" name="appliance_{{ $no }}" class="form-control">
                        <span class="input-group-addon">watt</span>
                    </div>
                </div>
            </div>
        @endfor
        <button type="submit" class="btn btn-default">Next Step</button>
    {{ Form::close() }}
@endif


{{-- handle step 3 --}}
@if($step == 3)
<h2 class="text-success">You have successfully calculated your tax</h2>
    <h3>Your tax is ₦{{ Session::get('tax') }}</h3>
    <p>Click <a href="{{ url('/') }}">here</a> to calculate a new one</p>
@endif



@endunless