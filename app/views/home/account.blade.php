@extends('layouts.main')

@section('content')

<div class="row">

@include('home.sidebar')

    <div class="col-xs-12 col-sm-6 col-md-8">
        <h2>Edit Your Account</h2>

        {{-- User Details--}}
        <?php $user = User::find(Auth::id())->first(); ?>

        @if (Session::has('errors'))
        <div class="alert alert-warning" role="alert">The following errors occurred</div>
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        @endif

        @if(Session::has('message'))
        <div class="alert alert-success" role="alert">Your Changes has been saved.</div>
        @endif

        {{ Form::open(array('action' => 'UserController@postIndex', 'role' => 'form')) }}

        <fieldset>
            <legend>About You</legend>

            <div class="form-group">
                {{ Form::label('first_name', 'First Name') }}
                {{ Form::text('first_name', $user->first_name, array('class'=>'form-control', 'id'=>'first_name', 'placeholder'=>'Enter Your First Name')) }}
            </div>

            <div class="form-group">
                {{ Form::label('last_name', 'Last Name') }}
                {{ Form::text('last_name', $user->last_name, array('class'=>'form-control', 'id'=>'last_name', 'placeholder'=>'Enter Your Last Name')) }}
            </div>

            <div class="form-group">
                {{ Form::label('sex', 'Sex') }}
                <select name="sex" class="form-control">
                    <option value="Male" @if($user->sex=='Male') selected  @endif>Male</option>
                    <option value="Female" @if($user->sex=='Female') selected  @endif>Female</option>
                </select>
            </div>

            <div class="form-group">
                {{ Form::label('salary', 'Salary') }}
                <div class="input-group">
                    <span class="input-group-addon">₦</span>
                    <input type="text" name="salary" class="form-control" placeholder="Salary" value="{{ $user->salary }}">
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('occupation', 'Occupation') }}
                {{ Form::select('occupation', array(
                'Teacher' => 'Teacher',
                'Trader' => 'Trader',
                'Banker' => 'Banker',
                'Nurse' => 'Nurse',
                'Medical Doctor' => 'Medical Doctor',
                'Lawyer' => 'Lawyer',
                'Civil Servant' => 'Civil Servant',
                'Dentist' => 'Dentist',
                'Engineer' => 'Engineer',
                'Chemist' => 'Chemist',
                'Nurse' => 'Nurse'
                ), $user->occupation, array('class'=>'form-control')) }}
            </div>
        </fieldset>


        <fieldset>
            <legend>Your Login Details</legend>
            <div class="form-group">
                {{ Form::label('email', 'Email address') }}
                {{ Form::email('email', $user->email, array('class'=>'form-control', 'id'=>'email', 'disabled'=>'disabled')) }}
            </div>

            <div class="form-group">
                <p>Leave blank if you don't want to change your password</p>
                <label for="password1">New Password</label>
                <input type="password" class="form-control" id="password1" name="password1" placeholder="Password">
            </div>

            <div class="form-group">
                <label for="password2">Confirm Password</label>
                <input type="password" class="form-control" id="password2" name="password2" placeholder="Retype your Password">
            </div>
        </fieldset>


        <button type="submit" class="btn btn-default">Submit</button>
        <br>
        {{ Form::close() }}
        <br>
        <br>

    </div>

</div>



@stop