@extends('layouts.main')

@section('content')

{{-- For successfully registration --}}
@if(Session::has('message'))
    <div class="alert alert-success" role="alert">Your Registration is successful.
        You can now proceed to <a href="{{ action('UserController@getLogin') }}">Login</a></div>
@endif

{{-- For successfully password reset --}}
@if (Session::has('status'))
<div class="alert alert-success" role="alert">
    {{ Session::get('status') }}
</div>
@endif


        {{-- check if user is logged in --}}
        @if(Auth::check())
<div class="row">

@include('home.sidebar')

    <div class="col-xs-12 col-sm-6 col-md-8">

        {{-- user is logged in... --}}
        {{-- include the app --}}
        @include('home.tax')

    </div>

</div>

@else

{{-- user is not logged in --}}
{{-- show to guest --}}
    <div class="jumbotron">
        <h1>Welcome Guest</h1>
        <p>Please <a href="{{ action('UserController@getLogin') }}">Login</a> or <a href="{{ action('UserController@getRegistration') }}">Register</a> to start calculating your tax</p>
        <p><a class="btn btn-primary btn-lg" role="button" href="{{ url('/about') }}">Learn more</a></p>
    </div>
@endif



@stop