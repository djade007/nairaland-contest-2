
@extends('layouts.main')

@section('content')


<h2>Quick Message</h2>
{{-- check if user is logged in --}}
@if(Auth::check())
    @include('home.sidebar')

@endif


<div class="col-xs-12 col-sm-6 col-md-8">
    <div class="row">
        <!-- Start: CONTACT US FORM -->
        <div class="col-lg-6">
            <form role="form">
                <div class="control-group">
                    <input type="text" id="inputName" placeholder="Name" class="form-control">
                </div>

                <div class="control-group">
                    <input type="text" id="inputEmail" placeholder="Email" class="form-control">
                </div>

                <div class="control-group">
                    <textarea id="inputMessage" placeholder="Message" class="form-control"></textarea>
                </div>

                <input type="submit" class="btn btn-primary btn-large" value="Send">
            </form>
        </div>

        <!-- End: CONTACT US FORM -->
    </div>

        <!-- Start: OFFICES -->
        <div class="span5 offset1">
            <div class="page-header">
                <h2>Offices</h2>
            </div>
            <h3>North America</h3>
            <div>
                <address class="pull-left">
                    <strong>Bootbusiness, Inc.</strong><br>
                    123 Folsom Ave, Suite 600<br>
                    USA<br>
                </address>
                <div class="pull-right">
                    <div class="bottom-space">
                        <i class="icon-phone icon-large"></i> (123) 123-1234
                    </div>
                    <a href="mailto:contact@nairanlandtaxapp.com" class="contact-mail">
                        <i class="icon-envelope icon-large"></i>
                    </a> contact@nairanlandtaxapp.com
                </div>
                <div class="clearfix"></div>
            </div>
            <h3>Europe</h3>
            <div>
                <address class="pull-left">
                    <strong>Bootbusiness, Inc.</strong><br>
                    123 Folsom Ave, Suite 600<br>
                    UK<br>
                </address>
                <div class="pull-right">
                    <div class="bottom-space">
                        <i class="icon-phone icon-large"></i> (123) 123-1234
                    </div>
                    <a href="mailto:contact@nairanlandtaxapp.com" class="contact-mail">
                        <i class="icon-envelope icon-large"></i>
                    </a> contact@nairanlandtaxapp.com
                </div>
                <div class="clearfix"></div>
            </div>
            <h3>Asia</h3>
            <div>
                <address class="pull-left">
                    <strong>Bootbusiness, Inc.</strong><br>
                    123 Folsom Ave, Suite 600<br>
                    India<br>
                </address>
                <div class="pull-right">
                    <div class="bottom-space">
                        <i class="icon-phone icon-large"></i> (123) 123-1234
                    </div>
                    <a href="mailto:contact@nairanlandtaxapp.com" class="contact-mail">
                        <i class="icon-envelope icon-large"></i>
                    </a> contact@nairanlandtaxapp.com
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- End: OFFICES -->
</div>


@stop

