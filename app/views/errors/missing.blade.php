<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>₦airaland Tax Calculator Application - not found</title>
    <meta name="description" content="404 Error" />
    <meta name="keywords" content="404, error" />
    <style>

    </style>
    <link rel="stylesheet" href="{{ asset('css/error-style.css') }}" />
</head>
<body>
<div class="wrapper">
    <div class="content">
        <div id="whoops"></div>
        <p>The page you are looking for seems to be missing.
            <br/>
            <a href="javascript:history.go(-1)">Go back</a>, or return to <a href="{{ url('/') }}">{{ url('/') }}</a> to choose a new direction.
            <br/>
            Please report any broken links to <a href="mailto:jide4real43@gmail.com">our team</a>.
        </p>
        <div id="divider"></div>
        <div class="menu">
            <a href="{{ url('/') }}">HOME</a><span class="separator">|</span>
            <a href="{{ url('/about') }}">ABOUT</a><span class="separator">|</span>
            <a href="{{ url('/account') }}">ACCOUNT</a><span class="separator">|</span>
            <a href="{{ url('/contact') }}">CONTACT</a>
        </div>
    </div>
</div>
</body>
</html>
