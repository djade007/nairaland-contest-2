@extends('layouts.main')

@section('content')



<h2>Login Form</h2>
<br>
@if (Session::has('errors'))
<div class="alert alert-danger" role="alert">
    Email or Password is incorrect
</div>
@endif

{{ Form::open(array('action' => 'UserController@postLogin', 'role' => 'form')) }}

<div class="form-group">
    {{ Form::label('email', 'Email address') }}
    {{ Form::email('email', Input::old('email'), array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Enter email')) }}
</div>

<div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
</div>

<div class="checkbox">
    <label>
        <input type="checkbox" name="remember">Remember me
    </label>
</div>
        <button type="submit" class="btn btn-default">Login</button> Forgotten password? click <a href="{{ action('UserController@getRemind') }}">here</a> to reset

        <br>
        <p>Don't have an account? <a href="{{ action('UserController@getRegistration') }}"><button type="button" class="btn btn-primary">Create Account</button></a</p>
{{ Form::close() }}



@stop