@extends('layouts.main')

@section('content')

@if (Session::has('errors'))
    <div class="alert alert-warning" role="alert">The following errors occurred</div>
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </div>
@endif

{{ Form::open(array('action' => 'UserController@postRegistration', 'role' => 'form')) }}

<fieldset>
    <legend>About You</legend>

    <div class="form-group">
        {{ Form::label('first_name', 'First Name') }}
        {{ Form::text('first_name', Input::old('first_name'), array('class'=>'form-control', 'id'=>'first_name', 'placeholder'=>'Enter Your First Name')) }}
    </div>

    <div class="form-group">
        {{ Form::label('last_name', 'Last Name') }}
        {{ Form::text('last_name', Input::old('last_name'), array('class'=>'form-control', 'id'=>'last_name', 'placeholder'=>'Enter Your Last Name')) }}
    </div>

    <div class="form-group">
        {{ Form::label('sex', 'Sex') }}
        <select name="sex" class="form-control">
            <option value="Male" @if(Input::old('sex')=='Male') selected  @endif>Male</option>
            <option value="Female" @if(Input::old('sex')=='Female') selected  @endif>Female</option>
        </select>
    </div>

    <div class="form-group">
        {{ Form::label('salary', 'Salary') }}
        <div class="input-group">
            <span class="input-group-addon">N</span>
            <input type="text" name="salary" class="form-control" placeholder="Salary" value="{{ Input::old('salary') }}">
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('occupation', 'Occupation') }}
        {{ Form::select('occupation', array(
            'Teacher' => 'Teacher',
            'Trader' => 'Trader',
            'Banker' => 'Banker',
            'Nurse' => 'Nurse',
            'Medical Doctor' => 'Medical Doctor',
            'Lawyer' => 'Lawyer',
            'Civil Servant' => 'Civil Servant',
            'Dentist' => 'Dentist',
            'Engineer' => 'Engineer',
            'Chemist' => 'Chemist',
            'Nurse' => 'Nurse'
        ), Input::old('occupation'), array('class'=>'form-control')) }}
</fieldset>


    <fieldset>
        <legend>Your Login Details</legend>
        <div class="form-group">
            {{ Form::label('email', 'Email address') }}
            {{ Form::email('email', Input::old('email'), array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Enter email')) }}
        </div>

        <div class="form-group">
            <label for="password1">Password</label>
            <input type="password" class="form-control" id="password1" name="password1" placeholder="Password">
        </div>

        <div class="form-group">
            <label for="password2">Confirm Password</label>
            <input type="password" class="form-control" id="password2" name="password2" placeholder="Retype your Password">
        </div>
    </fieldset>


<button type="submit" class="btn btn-default">Submit</button>
Already a Member? Click <a href="{{ action('UserController@getLogin') }}">Here</a> to Login
<br>
{{ Form::close() }}
<br>
<br>



@stop