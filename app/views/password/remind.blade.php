@extends('layouts.main')

@section('content')



<h2>Remind Password</h2>
<br>
@if (Session::has('errors'))
<div class="alert alert-danger" role="alert">
    {{ Session::get('errors') }}
</div>
@endif

@if (Session::has('status'))
<div class="alert alert-success" role="alert">
    {{ Session::get('status') }}
</div>
@endif

<form action="{{ action('UserController@postRemind') }}" method="POST"  role="form">
    {{-- generate a hidden form token --}}
    {{ Form::token() }}
    <div class="form-group">
        {{ Form::label('email', 'Email address') }}
        <input type="email" name="email" class="form-control" placeholder="Enter email">
    </div>
    <button type="submit" class="btn btn-default">Send Reminder</button>
</form>

@stop


