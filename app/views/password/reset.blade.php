@extends('layouts.main')

@section('content')



<h2>Reset Password</h2>
<br>
@if (Session::has('errors'))
<div class="alert alert-danger" role="alert">
    {{ Session::get('errors') }}
</div>
@endif
<form action="{{ action('UserController@postReset') }}" method="POST" role="form">
    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control">
    </div>

    <div class="form-group">
        <label for="password">New Password</label>
        <input type="password" name="password" class="form-control">
    </div>

    <div class="form-group">
        <label for="password_confirmation">Confirm Password</label>
        <input type="password" name="password_confirmation" class="form-control">
    </div>
    <button type="submit" class="btn btn-default">Reset Password</button>
</form>


@stop


