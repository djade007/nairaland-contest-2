<!DOCTYPE html>
<html lang="en">
<link rel='shortcut icon' type='image/x-icon' href="{{ url('/favicon.ico') }} >
    <title>Tax Calculator - {{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="page-header">
            <h1>₦airaland Tax Calculator Application <small>your no.1 tax calculator</small></h1>
        </div>

        <ul class="nav nav-pills nav-justified">
            {{-- add class active if it is on current page --}}
            <li @if($active == 'home') class="active" @endif><a href="{{ url('/') }}" title="Home">Home</a></li>
            <li @if($active == 'account') class="active" @endif><a href="{{ action('UserController@getIndex') }}" title="Edit your Account">Account</a></li>
            <li @if($active == 'contact') class="active" @endif><a href="{{ url('/contact') }}" title="Contact Us">Contact</a></li>
            <li @if($active == 'about') class="active" @endif><a href="{{ url('/about') }}" title="About Application">About</a></li>
        </ul>

        <hr>



        @yield('content')
    </div>

<footer class="bs-footer">
    <small>Developed By <a href="http://twitter.com/jide4real43">Jidez007</a></small>
</footer>
</body>
</html>