<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    //data to be passed to view
    $data = array(
        'title' => 'Home',
        'active' => 'home',
    );
	return View::make('home.index', $data);
});

Route::controller('account', 'UserController');

Route::controller('tax', 'TaxController');

Route::get('/contact', function() {

    //data to be passed to view
    $data = array(
        'title' => 'Contact Us',
        'active' => 'contact',
    );
    return View::make('home.contact', $data);
});

Route::get('/about', function() {

    //data to be passed to view
    $data = array(
        'title' => 'About App',
        'active' => 'about',
    );
    return View::make('home.about', $data);
});

// Handle 404 error
App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});