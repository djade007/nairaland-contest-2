<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    //Registration rules
    public static $rules = array(
        'first_name' => 'required|alpha_num|min:3|max:25',
        'last_name' => 'required|alpha_num|min:3|max:25',
        'sex' => 'required|in:Male,Female',
        'salary' => 'required|numeric|max:99999999.99',
        'occupation' => 'required',
        'email' => 'required|unique:users',
        'password1' => 'required|min:5',
        'password2' => 'same:password1'
    );

    // edit account rules
    public static $rules1 = array(
        'first_name' => 'required|alpha_num|min:3|max:25',
        'last_name' => 'required|alpha_num|min:3|max:25',
        'sex' => 'required|in:Male,Female',
        'salary' => 'required|numeric|max:99999999.99',
        'occupation' => 'required',
        'password1' => 'min:5',
        'password2' => 'required_with:password1|same:password1'
    );

    //Custom messages
    public static $messages = array(
        'password1.required' => 'The password field is required',
        'password1.min' => 'Your password must be at least 5 characters long',
        'password2.required_with' => 'You have to enter your new password twice',
        'password2.same' => 'The password does not match'

    );

}
