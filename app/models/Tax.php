<?php

class Tax extends Eloquent{

    // define the table
    protected $table = 'taxes';
    protected $primaryKey = 'user_id';
    protected $fillable = array('user_id', 'no_of_appliances', 'total_watt');

    // step1 rules
    public static $step1_rules = array(
        'no' => 'required|numeric|min:1|max:15'
    );

    // step2 rules
    public static $step2_rules = array(
        'appliance_1' => 'required|numeric',
        'appliance_2' => 'numeric',
        'appliance_3' => 'numeric',
        'appliance_4' => 'numeric',
        'appliance_5' => 'numeric',
        'appliance_6' => 'numeric',
        'appliance_7' => 'numeric',
        'appliance_8' => 'numeric',
        'appliance_9' => 'numeric',
        'appliance_10' => 'numeric',
        'appliance_11' => 'numeric',
        'appliance_12' => 'numeric',
        'appliance_13' => 'numeric',
        'appliance_14' => 'numeric',
        'appliance_15' => 'numeric'
    );
} 