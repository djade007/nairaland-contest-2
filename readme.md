# ₦airaland Tax Calculator Application Built with laravel 4.2

₦airaland Tax Calculator Application is a website created by [Olajide Afeez] (https://twitter.com/jide4real43) for nairaland programming challenge 2.

To see what this is about check out <http://www.nairaland.com/1816227/programming-challenge-beginners-competition-two>!

## Table of contents

 - [Purpose and Features](#purpose-and-features)
 - [Requirements](#requirements)
 - [Quick Start and Installation](#quick-start-and-installation)

## Purpose and Features

The purpose of this repository is for the judges from "Nairaland Programming Challenge For Beginners" to see my work.

The features of [₦airaland Tax Calculator Application] are:

- Login, Register and Recover lost password
- Calculate tax
- Show current salary and last tax

## Requirements

The ₦airaland Tax Calculator Application website requires a server with PHP 5.4+ that has the MCrypt extension installed.

The database engine that is used to store data for this application could be any of the engines supported by Laravel: MySQL, Postgres, SQLite and SQL Server.


## Quick Start and Installation

To get started: download this repository, create an empty database that this application will use, configure some settings in the app/config folder.


### Configuration

- Open up `app/config/database.php` and configure connection settings for your database.

After this simple configuration you can structure the database by running the few commands shown below.

### Installation

CD into the directory of this project and run the following two commands:

1. `composer install`
2. `php artisan migrate --force`

This will install all Composer dependencies, create the database structure.